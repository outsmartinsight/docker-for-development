#!/bin/bash

while [[ $# -gt 0 ]]; do
    PARAM="$1"
    case $PARAM in
        -h|--help)
            SHOW_HELP=1
        ;;

        --no-git-clone)
            SKIP_GIT_CLONE=1
        ;;

        --no-git-checkout)
            SKIP_GIT_CHECKOUT=1
        ;;

        *)
    esac
    shift
done

if [[ -n $SHOW_HELP ]]; then
    echo "$0: This script is used to bootstrap the build process for containers:"
    echo "  -h|--help           Show this help message"
    echo "  --no-git-clone      Do not clone project repository, assume it is already there"
    echo "  --no-git-checkout   Do not checkout project branch configured in .env"
    exit 0;
fi

# check if we have env file
if [[ ! -f ./.env ]]; then
    echo "Error: could not find confing.env in current path"
    exit 1;
fi

# load configs
. ./.env

# if we selected to not clone ...
if [[ -n $SKIP_GIT_CLONE ]]; then
    # ... and if the directory doesn't exist
    if [[ ! -d $LOCAL_APPS_DIR ]]; then
        echo "Error: you selected to not clone project, but the directory doesn't exist: $LOCAL_APPS_DIR"
        exit 1;
    else
        echo "Skipping cloning project in $LOCAL_APPS_DIR..."
    fi
# if we want to clone project...
else
    # check if we already have project directory, if true, will exit
    if [[ -d $LOCAL_APPS_DIR ]]; then
        echo "Error: project already exists on $LOCAL_APPS_DIR, either delete it or manually setup the docker containers"
        exit 1;
    fi

    git clone ssh://git@bitbucket.org:smarties/smarties-mk-2.git $LOCAL_APPS_DIR
fi

# if we want to run checkout to the specific branch
if [[ -z $SKIP_GIT_CHECKOUT ]]; then
    cd $LOCAL_APPS_DIR
    git checkout $GIT_INITIAL_BRANCH
    cd -
fi

docker-compose build
