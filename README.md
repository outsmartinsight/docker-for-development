Docker development environment
=================

Install(for Linux)
---------------

1. Download and install Docker: https://www.docker.com/products/overview
3. Install Docker Compose here: https://docs.docker.com/compose/install/
4. Copy ``.env.sample`` file into ``.env`` and edit it(see section below for env variables specs):
5. Add your **smarties.dev** host to the ``/etc/hosts`` file
```
    127.0.0.1 smarties.dev
```
6. Run ``bin/build.sh``(see useful commands section for available options)
7. After you get the message that the setup has ended, you can proceed to configure your project config files
8. Run ``bin/run.sh`` to start the containers(see useful commands section for available options)

Enviroment file variables
-------------------
1. ``LOCAL_APPS_DIR`` the directory where the project source code will be stored.
2. ``WSGI_APP`` django wsgi module
3. ``WORKERS`` number of workers to start gunicorn with
4. ``PORT`` port to run gunicorn on
5. ``SECRET_KEY`` secret key for django app
6. ``DB_NAME`` database name
7. ``DB_USER`` database user
8. ``DB_PASS`` database password
9. ``DB_SERVICE`` database host
10. ``DB_PORT`` database port
11. ``DEBUG`` debug mode in django app
12. ``ENV`` django app enviroment. It tells django app what setting file to load.
13. ``STATIC_ROOT`` static files root directory
14. ``PUBLIC_MEDIA_ROOT`` public media files root directory
15. ``PRIVATE_MEDIA_ROOT`` private media files root directory
16. ``POSTGRES_DB`` postgresql database name (usually should be same as DB_NAME)
17 ``POSTGRES_USER`` postgresql database user (usually should be same as DB_USER)
18. ``POSTGRES_PASSWORD`` postgresql database password (usually should be same as DB_NAME)
19. ``NODE_ENV`` enviroment for react app
20. ``WEBPACK_URL`` webpack url for react app
21. ``GIT_INITIAL_BRANCH`` (optional) the initial branch of the project repository we want to checkout
22. ``DOCKER_SOCKET_PATH`` path to docker socket
23: ``LOCAL_USER_ID`` user id of local machine. this is used to manage the files inside the containers as same user as the local machine. By default a docker container handles the files as root user and they ain't acceesible on local machine  
24: ``ALLOWED_HOST`` host/domain names that this Django site can serve

Useful commands
-------------------
* ``bin/build.sh`` install the project
```
bin/build.sh: This script is used to bootstrap the build process for containers:
  -h|--help           Show this help message
  --no-git-clone      Do not clone project repository, assume it is already there
  --no-git-checkout   Do not checkout project branch configured in .env
```
* ``bin/run.sh`` start the docker containers
```
Builds, (re)creates, starts, and attaches to containers for a service.

Unless they are already running, this command also starts any linked services.

The `docker-compose up` command aggregates the output of each container. When
the command exits, all containers are stopped. Running `docker-compose up -d`
starts the containers in the background and leaves them running.

If there are existing containers for a service, and the service's configuration
or image was changed after the container's creation, `docker-compose up` picks
up the changes by stopping and recreating the containers (preserving mounted
volumes). To prevent Compose from picking up changes, use the `--no-recreate`
flag.

If you want to force Compose to stop and recreate all containers, use the
`--force-recreate` flag.

Usage: up [options] [SERVICE...]

Options:
    -d                         Detached mode: Run containers in the background,
                               print new container names.
                               Incompatible with --abort-on-container-exit.
    --no-color                 Produce monochrome output.
    --no-deps                  Don't start linked services.
    --force-recreate           Recreate containers even if their configuration
                               and image haven't changed.
                               Incompatible with --no-recreate.
    --no-recreate              If containers already exist, don't recreate them.
                               Incompatible with --force-recreate.
    --no-build                 Don't build an image, even if it's missing.
    --build                    Build images before starting containers.
    --abort-on-container-exit  Stops all containers if any container was stopped.
                               Incompatible with -d.
    -t, --timeout TIMEOUT      Use this timeout in seconds for container shutdown
                               when attached or when containers are already
                               running. (default: 10)
    --remove-orphans           Remove containers for services not
                               defined in the Compose file
```
* ``bin/console.sh`` django console
* ``docker-compose up`` will start the cluster (add --build if you want to build it)
* ``docker ps`` will list all live containers (you will also get the container id here)
* ``docker stop`` or ``docker kill`` to stop or kill a container
* ``docker exec -ti <containerId> /bin/bash`` will give you a bash prompt on the specified container (you can use it to
run any command, not only bash, if you don't need the prompt)
* check out also ``docker rm`` and ``docker rmi``

Container structure
--------------------
Currently, there are these images:

* **smarties_platform**: this contains the platform source code served by gunicorn
* **smarties_load_balancer**: this contains the nginx server that will call apache. The TLS certificate is only available at this level (project container will not have https)
* **smarties_db**: this is where the DB is being hosted
* **smarties_mailcatcher** - this is a mailtrap to help us test the emails in development. the smtp configurations are:
** HOST: cc_mailcatcher
** PORT: 1025
You can check the emails at http://smarties.dev:1080 . N.B.: the emails are not persistent. they get cleared once the container is stoped.


TODO
------------------

* Refactor nginx conf template to get the hostname from .env file